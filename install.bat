@echo off
wget https://github.com/alexa-pi/AlexaPi/archive/master.zip
wget https://github.com/jraspiprojects/AlexaPi-Windows/raw/master/unzip.exe
unzip master.zip
cd AlexaPi-master
cd src
cd scripts
call setup.bat
